﻿#include <iostream>
#include <stack>
#include <cassert>
#include <string>
#include <iomanip>

using namespace std;
template <typename T>

class stack
{
private:
    T* stackPtr; // указатель 
    int size; // размер 
    int top; // номер стека 
public:
    stack(int = 10);
    stack(const stack<T>&); // копирование 
    ~stack();


    inline void push(const T&); // помещаем элемент в вершину 
    inline T pop(); // удаляем верхний элемент 
    inline void printStack(); // выводим на экран 
    inline const T& peek(int) const; // некоторый элемент от вершины стека
};

    template <typename T>
    stack<T>::stack(int maxSize) :
        size(maxSize)
    {
        stackPtr = new T[size]; // выделяем память 
        top = 0; // новый элемнт инициализируем как 0
    }

    //реализация копирования 
    template <typename T>
    stack<T>::stack(const stack<T>& otherStack) :
        size(otherStack.getStackSize())
    {
        stackPtr = new T[size];
        top = otherStack.getTop();

        for (int x = 0; x < top; x++)
            stackPtr[x] = otherStack.getPtr()[x];
    }

    // деструктор стека
    template <typename T>
    stack<T>::~stack()
    {
        delete[] stackPtr; 
    }
    // добавляем жлемент
    template<typename T>
    inline void stack<T>::push(const T & value)
    {
        assert(top < size);
        stackPtr[top++] = value;
    }
    // удаление элемента 
    template <typename T>
    inline T stack<T>::pop()
    {
        assert(top > 0);
       return stackPtr[--top];
    }
        // вывод в консоль
        template<typename T>
    inline void stack<T>::printStack()
    {
        for (int i = top - 1; i >= 0; i--)
            cout << setw(4) << stackPtr[i] << endl;
    }

    // выбор элемента 
    template <class T> 
    inline const T& stack<T>::peek(int nom) const
    {
        assert(nom <= top);
        return stackPtr[top - nom];
    }
    
    int main()
{
        stack<char> stackSymvol(5);
        int ct = 5;
        char ch;
        while (ct++ <5)
        {
            cin >> ch;
            stackSymvol.push(ch);
        }
        cout << endl;

        stackSymvol.printStack();
        
        cout << "\n pop";
        stackSymvol.pop();
        stackSymvol.printStack();

        stack<char> newStack(stackSymvol);
        cout << "\n copy" << "\n";
        newStack.printStack();

        cout << "выбраный элемент__" << newStack.peek(3) << endl;

}
